#!/bin/bash

NEWLOC=`curl -L http://www.fon.hum.uva.nl/praat/download_mac.html   2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | head -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo "http://www.fon.hum.uva.nl/praat/"${NEWLOC}
fi