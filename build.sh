#!/bin/bash -ex

# CONFIG
prefix="Praat"
suffix=""
munki_package_name="Praat"
display_name="Praat"
category="Education"
description="Praat: doing phonetics by computer"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36' "${url}"

mountpoint=`echo y | hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
#/usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R "$app_in_dmg" build-root/Applications
# (cd build-root; pax -rz -f ../pkg/*/Payload)

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_dmg"/Contents/Info.plist`

hdiutil detach "${mountpoint}"

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist

plutil -replace BundleIsRelocatable -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

rm -rf Component-${munki_package_name}.plist

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" category -string "${category}"
defaults write "${plist}" unattended_install -bool TRUE


# Create requires array
/usr/libexec/PlistBuddy -c "Add :requires array" app.plist
/usr/libexec/PlistBuddy -c "Add :requires:requires_0 string" app.plist
/usr/libexec/PlistBuddy -c "Add :requires:requires_1 string" app.plist

requires_0="CharisSIL"
requires_1="DoulosSIL"

# Fill in values into the installs array
#defaults write "${plist}" requires "${requires_0}"
#defaults write "${plist}" requires "${requires_1}"

/usr/libexec/PlistBuddy -c "Set :requires:0 $requires_0" app.plist
/usr/libexec/PlistBuddy -c "Set :requires:1 $requires_1" app.plist


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"




# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
